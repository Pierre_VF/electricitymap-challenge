"""
This module contains methods to compute emission intensities.
"""
from pandas import DataFrame


def get_emission_factors() -> DataFrame:
    """
    This method returns emission factors from different generator technologies (using electricityMap data)

    :return: emission intensities in Dataframe format, indexed by technology name
    """

    # This is copy-pasted from electricityMap's Git
    # https://github.com/electricityMap/electricitymap-contrib/blob/master/config/co2eq_parameters.json#L2-L66
    emission_factors = {
        "battery charge": {
            "_comment": "Emissions are counted at discharge",
            "source": "Tomorrow",
            "value": 0,
        },
        "battery discharge": {
            "_comment": "As a default fallback, we use here the annual mean carbon intensity of all electricityMap zones",
            "source": "World average intensity by Tomorrow",
            "value": 301.13684338512996,
        },
        "biomass": {"source": "IPCC 2014", "value": 230},
        "coal": {"source": "IPCC 2014", "value": 820},
        "gas": {"source": "IPCC 2014", "value": 490},
        "geothermal": {"source": "IPCC 2014", "value": 38},
        "hydro": {"source": "IPCC 2014", "value": 24},
        "hydro charge": {
            "_comment": "Emissions are counted at discharge",
            "source": "Tomorrow",
            "value": 0,
        },
        "hydro discharge": {
            "_comment": "As a default fallback, we use here the annual mean carbon intensity of all electricityMap zones",
            "source": "World average intensity by Tomorrow",
            "value": 301.13684338512996,
        },
        "nuclear": {"source": "IPCC 2014", "value": 12},
        "oil": {
            "_comment": "UK Parliamentary Office of Science and Technology (2006) 'Carbon footprint of electricity generation'",
            "source": "UK POST 2014",
            "value": 650,
        },
        "solar": {"source": "IPCC 2014", "value": 45},
        "unknown": {
            "_comment": "assume conventional",
            "source": "assumes thermal (coal, gas, oil or biomass)",
            "value": 700,
        },
        "wind": {"source": "IPCC 2014", "value": 11},
    }

    df_generators = DataFrame(emission_factors).T
    return df_generators


def compute_emission_intensity(df: DataFrame) -> DataFrame:
    """
    Method to evaluate the emission intensity given a production mix (imports/exports are neglected)

    :param df: dataframe of the production
    :return: input dataframe extended with emission intensity (average_co2_intensity - in g CO2eq/kWh)
    """

    emission_factors = get_emission_factors()

    df["total_production"] = 0
    df["total_co2_emissions"] = 0
    for c in emission_factors.index:
        if c in df.keys():
            factor_c = emission_factors.loc[c, "value"]
            if not df[c].isna().all():
                df["total_production"] = df["total_production"] + df[c]
                df["total_co2_emissions"] = df["total_co2_emissions"] + factor_c * df[c]

    # Computing emission intensity, seen from the demand side (assuming 5% losses in transmission/distribution)
    df["total_demand"] = df["total_production"] * 0.95
    df["average_co2_intensity"] = df["total_co2_emissions"] / df["total_demand"]

    return df
